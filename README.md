# Authoring Terrains with Spatialised Style

![](docs/teaser.jpg)

This repository contains the official code and resources for the paper *[Authoring Terrains with Spatialised Style](https://simonperche.github.io/authoring_terrains_with_spatialised_style)*. 

# Structure

The repository is divided into three folders :
- data: where you can download data for the training with associated scripts
- generator: generator code based on StyleGAN2
- encoders: encoders code based on Pixel2Style2Pixel

Please train a generator before training encoders. You can also use [our pre-trained StyleGAN2 models](https://drive.google.com/drive/folders/1fuZDELhNvHpZ9k8VaqvUfICX2HyGDf1A?usp=drive_link).
Each folder contains instructions for running the code.

A Blender addon has been developed for this paper. Please find the code and pretrained models [here](https://gitlab.liris.cnrs.fr/sperche/styledem-blender-addon).

# Citation
If you find our work useful, please consider citing:

```
@article{perche2023spatialisedstyle,
  author    = {Perche, Simon and Peytavie, Adrien and Benes, Bedrich and Galin, Eric and Guérin, Eric},
  title     = {Authoring Terrains with Spatialised Style},
  journal   = {Pacific Graphics},
  year      = {2023},
}
```


## License

This project is licensed under the MIT License. See the [LICENSE](LICENSE) file for details.
