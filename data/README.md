# Authoring Terrains with Spatialised Style

This folder contains scripts to export downloaded data into 16-bit png files.
Due to licensing, we cannot provide direct download links. 
Please note that for using pretrained models, data is not necessary.

# Usage

Three models were trained for our paper: 5 meters/pixel, 30 meters/pixel and 180 meters/pixel at 1024x1024 resolution.

## IGN (5 meters per pixel)
You will find the data on the [IGN website](https://geoservices.ign.fr/rgealti#telechargement5m). This website contains all regions of France at 5m/pixel 1000x1000 images.
The file format is .asc. You can convert this to .png using :
```
python asc2png.py -f=asc_folder/ -o=output_folder --width=1024 --height=1024 --normalize
```

## SRTM
### 30 meters per pixel
You will find the data on the [NASA website](https://search.earthdata.nasa.gov/search?q=SRTM). Select `NASA Shuttle Radar Topography Mission Global 1 arc second V003`. The images are 4000x4000 pixels at 30m/pixel.
The file format is .hgt. You can convert this to .png using :
```
python hgt2png.py -f=asc_folder/ -o=output_folder --width=1024 --height=1024 --normalize --crop
```

### 180 meters per pixel
Please follow the same steps as 30m/pixel but choose `NASA Shuttle Radar Topography Mission Global 3 arc second`. This will produce 90m/pixel image. To get 180m/pixel, downsample them by 2.

# Citation
If you find our work useful, please consider citing:

```
@article{perche2023spatialisedstyle,
  author    = {Perche, Simon and Peytavie, Adrien and Benes, Bedrich and Galin, Eric and Guérin, Eric},
  title     = {Authoring Terrains with Spatialised Style},
  journal   = {Pacific Graphics},
  year      = {2023},
}
```

## License

This project is licensed under the MIT License. See the [LICENSE](../LICENSE) file for details.
