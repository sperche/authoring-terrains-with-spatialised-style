import math
import os
import argparse
import shutil

import cv2
import numpy as np
import py7zr


def main():
    parser = argparse.ArgumentParser('Convert tif files to png 16 bits images.')
    parser.add_argument('-f', '--folder', type=str, help='Folder of .hgt files', default='')
    parser.add_argument('--zip', type=str, help='Zipfile containing .asc files', default='')
    parser.add_argument('-o', '--output', type=str, help='Output folder of .png files', default='out/')
    parser.add_argument('--width', type=int, help='Width of the output image')
    parser.add_argument('--height', type=int, help='Width of the output image')
    parser.add_argument('--normalize', action='store_true', help='If present, normalize images before saving.')
    parser.add_argument('--crop', action='store_true', help='If present, crop instead of resizing.')

    args = parser.parse_args()

    assert bool(args.folder) ^ bool(args.zip), 'Must set either --folder or --zip (not both)'

    i = 0
    step = 50

    if args.folder:
        for filename in os.listdir(args.folder):
            file = os.path.join(args.folder, filename)
            if os.path.join(file):
                hgt2png(file, args.output, args.width if args.width else None, args.height if args.height else None,
                        args.normalize, args.crop)
            i += 1
            if i % step == 0:
                print(i)
    elif args.zip:
        with py7zr.SevenZipFile(args.zip) as z:
            names = [n for n in z.getnames() if n.endswith('.asc')]
            z.extract(targets=names)
            for n in names:
                hgt2png(n, args.output, args.width if args.width else None,
                        args.height if args.height else None, args.normalize, args.crop)
            shutil.rmtree(names[0].split('/')[0])
            i += 1
            if i % step == 0:
                print(i)


def hgt2png(filepath, out_folder, width, height, normalize, crop):
    if out_folder and not os.path.isdir(out_folder):
        os.mkdir(out_folder)

    folder = '/'.join(filepath.split(os.sep)[:-1])
    filename = filepath.split(os.sep)[-1].split('.')[0]

    file = os.path.join(folder, f'{filename}.hgt')
    siz = os.path.getsize(file)
    dim = int(math.sqrt(siz / 2))

    assert dim * dim * 2 == siz, 'Invalid file size'

    img = np.fromfile(file, np.dtype('>i2'), dim * dim).reshape((dim, dim))

    if normalize:
        img = ((img - np.min(img)) / (np.max(img) - np.min(img))) * 65535

    img = img.astype(np.uint16)

    if crop:
        assert bool(width) & bool(height)
        n_x = img.shape[0] // height
        n_y = img.shape[1] // width
        for i in range(n_x):
            for j in range(n_y):
                crop_img = img[i*height:i*height+height, j*width:j*width+width]
                if normalize:
                    crop_img = ((crop_img - np.min(crop_img)) / (np.max(crop_img) - np.min(crop_img))) * 65535
                out = os.path.join(out_folder, f'{filename}_{i:04d}_{j:04d}.png')
                cv2.imwrite(out, crop_img.astype(np.uint16))
    else:
        if width and height:
            img = cv2.resize(img, (width, height))

        out = os.path.join(out_folder, f'{filename}.png')
        cv2.imwrite(out, img.astype(np.uint16))


if __name__ == '__main__':
    main()
