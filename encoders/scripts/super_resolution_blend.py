import os
from argparse import Namespace
from copy import deepcopy
import shutil

from tqdm import tqdm
import numpy as np
import torch
import sys
from natsort import natsorted
from sklearn.linear_model import LinearRegression

sys.path.append(".")
sys.path.append("..")

from options.test_options import TestOptions
from models.psp import pSp
import cv2

device = 'cuda' if torch.cuda.is_available() else 'cpu'


# Infer super resolution using in-between images between rows AND columns
def run():
    test_opts = TestOptions()
    test_opts.parser.add_argument('--low_res_image', type=str, help='Path to low resolution image')
    test_opts.parser.add_argument('--make_collage', action='store_true',
                                  help='Create a collage of all inferences using Wyvill blending.')
    test_opts.parser.add_argument('--keep_tmp_folders', action='store_true',
                                  help='If present, do not delete temporary folders with intermediate images.')
    test_opts = test_opts.parse()

    if test_opts.resize_factors is not None:
        assert len(
            test_opts.resize_factors.split(',')) == 1, "When running inference, provide a single downsampling factor!"
        out_path_results = os.path.join(test_opts.exp_dir, 'inference_results',
                                        'downsampling_{}'.format(test_opts.resize_factors))
    else:
        out_path_results = os.path.join(test_opts.exp_dir, 'inference_results')

    tmp_crop_dir = os.path.join(test_opts.exp_dir, 'tmp_crop')
    tmp_inference_dir = os.path.join(test_opts.exp_dir, 'tmp_inference')

    os.makedirs(out_path_results, exist_ok=True)
    os.makedirs(tmp_crop_dir, exist_ok=True)
    os.makedirs(tmp_inference_dir, exist_ok=True)

    shutil.copy(test_opts.low_res_image, os.path.join(test_opts.exp_dir, 'low_res_input.png'))

    # update test options with options used during training
    ckpt = torch.load(test_opts.checkpoint_path, map_location='cpu')
    opts = ckpt['opts']
    opts.update(vars(test_opts))
    if 'learn_in_w' not in opts:
        opts['learn_in_w'] = False
    if 'output_size' not in opts:
        opts['output_size'] = 1024
    opts = Namespace(**opts)

    net = pSp(opts).to(device)
    net.eval()

    sr_grid = opts.sr_grid

    low_res_image = cv2.imread(test_opts.low_res_image, cv2.IMREAD_UNCHANGED)
    crop(test_opts.low_res_image, tmp_crop_dir, low_res_image.shape[0]//sr_grid, 1024, False)

    files = natsorted([os.path.join(tmp_crop_dir, f) for f in os.listdir(tmp_crop_dir)
                       if os.path.isfile(os.path.join(tmp_crop_dir, f)) and f.endswith('.png')])

    print('Inferring...')
    for i in tqdm(range(((sr_grid*2)-1)**2)):
        # To build in-between images, consecutive files are supposed to be on a grid row by row
        row, col = np.unravel_index(i, ((sr_grid*2)-1, (sr_grid*2)-1))

        # Simple case : all real_im
        if row % 2 == 0 and col % 2 == 0:
            file = files[(row//2) * sr_grid + (col // 2)]
            real_im = cv2.imread(file, cv2.IMREAD_UNCHANGED).astype(np.float64)
            from_im = real_im
        elif col % 2 != 0 and row % 2 == 0:  # Half of the left image, half of the right
            left_img = files[(row // 2) * sr_grid + ((col-1) // 2)]
            right_img = files[(row // 2) * sr_grid + ((col+1) // 2)]

            left_img = cv2.imread(left_img, cv2.IMREAD_UNCHANGED).astype(np.float64)
            right_img = cv2.imread(right_img, cv2.IMREAD_UNCHANGED).astype(np.float64)

            from_im = np.empty(left_img.shape)

            middle = left_img.shape[1] // 2
            from_im[:, :middle] = left_img[:, middle:]
            from_im[:, middle:] = right_img[:, :middle]
        elif col % 2 == 0 and row % 2 != 0:  # Half of the above image, half of the below image
            above_img = files[((row-1) // 2) * sr_grid + (col// 2)]
            below_img = files[((row+1) // 2) * sr_grid + (col // 2)]

            above_img = cv2.imread(above_img, cv2.IMREAD_UNCHANGED).astype(np.float64)
            below_img = cv2.imread(below_img, cv2.IMREAD_UNCHANGED).astype(np.float64)

            from_im = np.empty(above_img.shape)

            middle = above_img.shape[0] // 2
            from_im[:middle, :] = above_img[middle:, :]
            from_im[middle:, :] = below_img[:middle, :]
        elif row % 2 != 0 and col % 2 != 0:  # A quarter of the up left, up right, down left and down right image
            up_left = files[((row - 1) // 2) * sr_grid + ((col - 1) // 2)]
            up_right = files[((row - 1) // 2) * sr_grid + ((col + 1) // 2)]
            down_left = files[((row + 1) // 2) * sr_grid + ((col - 1) // 2)]
            down_right = files[((row + 1) // 2) * sr_grid + ((col + 1) // 2)]

            up_left = cv2.imread(up_left, cv2.IMREAD_UNCHANGED).astype(np.float64)
            up_right = cv2.imread(up_right, cv2.IMREAD_UNCHANGED).astype(np.float64)
            down_left = cv2.imread(down_left, cv2.IMREAD_UNCHANGED).astype(np.float64)
            down_right = cv2.imread(down_right, cv2.IMREAD_UNCHANGED).astype(np.float64)

            from_im = np.empty(up_left.shape)

            middle_y = up_left.shape[0] // 2
            middle_x = up_left.shape[1] // 2

            from_im[:middle_y, :middle_x] = up_left[middle_y:, middle_x:]
            from_im[:middle_y, middle_x:] = up_right[middle_y:, :middle_x]
            from_im[middle_y:, :middle_x] = down_left[:middle_y, middle_x:]
            from_im[middle_y:, middle_x:] = down_right[:middle_y, :middle_x]

        input_img = prepare_img(from_im)
        result = net(input_img, randomize_noise=opts.randomize_noise, resize=opts.resize_outputs)
        result = result[0].cpu().detach().transpose(0, 2).transpose(0, 1).numpy()
        result = normalize(result[:, :, 0], 65535)

        result = retarget_dynamic(result, from_im).astype(np.uint16)

        im_save_path = os.path.join(tmp_inference_dir, f'{os.path.basename(file)}_{i}.png')
        cv2.imwrite(im_save_path, result)

    print('Blending...')
    blend_intermediate(tmp_inference_dir, out_path_results, sr_grid)

    if test_opts.make_collage:
        print('Creating the final image...')
        make_collage(out_path_results, os.path.join(test_opts.exp_dir, 'final.png'), sr_grid)

    if not test_opts.keep_tmp_folders:
        shutil.rmtree(tmp_crop_dir)
        shutil.rmtree(tmp_inference_dir)


def prepare_img(img: np.ndarray):
    img = normalize(img.astype(np.float64))
    if img.shape[0] != 256:
        img = cv2.resize(img, (256, 256))
    img = torch.from_numpy(img)

    if len(img.shape) == 2:
        img = torch.unsqueeze(img, 0)

    return torch.unsqueeze(img, 0).float().to(device)


def crop(in_img: str, out_folder: str, size: int, resize: int, normalize_output: bool):
    if not os.path.isdir(out_folder):
        os.mkdir(out_folder)

    img = cv2.imread(in_img, cv2.IMREAD_UNCHANGED)

    n_x = img.shape[0] // size
    n_y = img.shape[1] // size
    for i in range(n_x):
        for j in range(n_y):
            crop_img = img[i * size:i * size + size, j * size:j * size + size]
            if resize is not None:
                crop_img = cv2.resize(crop_img, (resize, resize))
            if normalize_output:
                crop_img = normalize(crop_img, 65535)
            out = os.path.join(out_folder, f'{i:04d}_{j:04d}.png')
            cv2.imwrite(out, crop_img.astype(np.uint16))


def retarget_dynamic(x: np.ndarray, y: np.ndarray):
    x_flat = x.reshape((-1, 1))
    y_flat = y.flatten()
    model = LinearRegression().fit(x_flat, y_flat)
    res = x * model.coef_ + model.intercept_

    # Overflow might happen if min or max of y are around the range limits
    if np.min(res) < 0 and np.max(res) > 65535:
        res = normalize(res, 65535)
    elif np.min(res) < 0:
        res = ((res - np.min(res)) / (65535 - np.min(res))) * 65535
    elif np.max(res) > 65535:
        res = ((res - 0) / (np.max(res) - 0)) * 65535

    return res.reshape(x.shape)


def normalize(array: np.ndarray, high_range: float = 1):
    if np.max(array) - np.min(array) == 0:
        return np.zeros(array.shape)
    return ((array - np.min(array)) / (np.max(array) - np.min(array))) * high_range


def blend_intermediate(in_folder: str, out_folder: str, sr_grid_size: int):
    if not os.path.isdir(out_folder):
        os.mkdir(out_folder)

    files = natsorted([os.path.join(in_folder, f) for f in os.listdir(in_folder)
                       if os.path.isfile(os.path.join(in_folder, f)) and f.endswith('.png')])

    sr_img = cv2.imread(files[0], cv2.IMREAD_UNCHANGED)
    mask = np.empty(sr_img.shape)

    d_max = np.sqrt((sr_img.shape[0] // 2 - 0) ** 2)

    def wyvill(x):
        if x < 0:
            return 0
        elif x > 1:
            return 1

        return -(4 / 9) * x * x * x * x * x * x + (17 / 9) * x * x * x * x - (22 / 9) * x * x + 1

    inner_radius = 0.2
    outer_radius = 0.8
    for i in range(sr_img.shape[0]):
        for j in range(sr_img.shape[1]):
            # Circle
            d = np.sqrt((i - sr_img.shape[0] // 2) ** 2 + (j - sr_img.shape[1] // 2) ** 2)
            d /= d_max

            d = (d - inner_radius) / (outer_radius - inner_radius) if inner_radius < d < outer_radius else 0 if d < inner_radius else 1
            d = wyvill(d)

            mask[i, j] = d

    m_y = sr_img.shape[0] // 2
    m_x = sr_img.shape[1] // 2

    final_mask = deepcopy(mask)
    final_mask[:m_y, :m_x] += mask[m_y:, m_x:] + mask[m_y:, :m_x] + mask[:m_y, m_x:]
    final_mask[:m_y, m_x:] += mask[m_y:, m_x:] + mask[m_y:, :m_x] + mask[:m_y, :m_x]
    final_mask[m_y:, :m_x] += mask[m_y:, m_x:] + mask[:m_y, m_x:] + mask[:m_y, :m_x]
    final_mask[m_y:, m_x:] += mask[m_y:, :m_x] + mask[:m_y, m_x:] + mask[:m_y, :m_x]

    cv2.imwrite(os.path.join(out_folder, f'mask.png'), (mask*255).astype(np.uint8))

    inference_grid_size = (sr_grid_size * 2) - 1
    for i in tqdm(range(((sr_grid_size * 2) - 1) ** 2)):
        # To build in-between images, consecutive files are supposed to be on a grid row by row
        row, col = np.unravel_index(i, ((sr_grid_size * 2) - 1, (sr_grid_size * 2) - 1))

        # Only blend real images (not intermediate)
        if row % 2 != 0 or col % 2 != 0:
            continue

        middle = files[row * inference_grid_size + col]
        imgs = np.zeros((sr_img.shape[0], sr_img.shape[1], 9))
        imgs[:, :, 4] = cv2.imread(middle, cv2.IMREAD_UNCHANGED)

        # Up, down, right and left cases
        if row != 0:
            up = files[(row - 1) * inference_grid_size + col]
            imgs[:, :, 1] = cv2.imread(up, cv2.IMREAD_UNCHANGED).astype(np.float64)
        else:
            imgs[m_y:, :, 1] += imgs[:m_y, :, 4]

        if col != 0:
            left = files[row * inference_grid_size + (col - 1)]
            imgs[:, :, 3] = cv2.imread(left, cv2.IMREAD_UNCHANGED).astype(np.float64)
        else:
            imgs[:, m_x:, 3] += imgs[:, :m_x, 4]

        if col != inference_grid_size - 1:
            right = files[row * inference_grid_size + (col + 1)]
            imgs[:, :, 5] = cv2.imread(right, cv2.IMREAD_UNCHANGED).astype(np.float64)
        else:
            imgs[:, :m_x, 5] += imgs[:, m_x:, 4]

        if row != inference_grid_size - 1:
            down = files[(row + 1) * inference_grid_size + col]
            imgs[:, :, 7] = cv2.imread(down, cv2.IMREAD_UNCHANGED).astype(np.float64)
        else:
            imgs[:m_y, :, 7] += imgs[m_y:, :, 4]

        # Up-left, down-left, up-right and down-right cases
        # It might probably be rewritten more concisely but I find it clearer (sort of) to detail each case
        if col != 0 and row != 0:
            up_left = files[(row - 1) * inference_grid_size + (col - 1)]
            imgs[:, :, 0] = cv2.imread(up_left, cv2.IMREAD_UNCHANGED).astype(np.float64)
        elif col == 0 and row == 0:
            imgs[m_y:, m_x:, 0] += imgs[:m_y, :m_x, 4]
        elif col != 0 and row == 0:
            imgs[m_y:, m_x:, 0] += imgs[:m_y, m_x:, 3]
        elif col == 0 and row != 0:
            imgs[m_y:, m_x:, 0] += imgs[m_y:, :m_x, 1]

        if col != inference_grid_size - 1 and row != 0:
            up_right = files[(row - 1) * inference_grid_size + (col + 1)]
            imgs[:, :, 2] = cv2.imread(up_right, cv2.IMREAD_UNCHANGED).astype(np.float64)
        elif col == inference_grid_size - 1 and row == 0:
            imgs[m_y:, :m_x, 2] += imgs[:m_y, m_x:, 4]
        elif col != inference_grid_size - 1 and row == 0:
            imgs[m_y:, :m_x, 2] += imgs[:m_y, :m_x, 5]
        elif col == inference_grid_size - 1 and row != 0:
            imgs[m_y:, :m_x, 2] += imgs[m_y:, m_x:, 1]

        if col != 0 and row != inference_grid_size - 1:
            down_left = files[(row + 1) * inference_grid_size + (col - 1)]
            imgs[:, :, 6] = cv2.imread(down_left, cv2.IMREAD_UNCHANGED).astype(np.float64)
        elif col == 0 and row == inference_grid_size - 1:
            imgs[:m_y, m_x:, 6] += imgs[m_y:, :m_x, 4]
        elif col == 0 and row != inference_grid_size - 1:
            imgs[:m_y, m_x:, 6] += imgs[:m_y, :m_x, 7]
        elif col != 0 and row == inference_grid_size - 1:
            imgs[:m_y, m_x:, 6] += imgs[m_y:, m_x:, 3]

        if col != inference_grid_size - 1 and row != inference_grid_size - 1:
            down_right = files[(row + 1) * inference_grid_size + (col + 1)]
            imgs[:, :, 8] = cv2.imread(down_right, cv2.IMREAD_UNCHANGED).astype(np.float64)
        elif col == inference_grid_size - 1 and row == inference_grid_size - 1:
            imgs[:m_y, :m_x, 8] += imgs[m_y:, m_x:, 4]
        elif col == inference_grid_size - 1 and row != inference_grid_size - 1:
            imgs[:m_y, :m_x, 8] += imgs[:m_y, m_x:, 7]
        elif col != inference_grid_size - 1 and row == inference_grid_size - 1:
            imgs[:m_y, :m_x, 8] += imgs[m_y:, :m_x, 5]

        imgs *= mask[:, :, np.newaxis]

        final_img = imgs[:, :, 4]

        final_img[:m_y, :m_x] += imgs[m_y:, m_x:, 0] + imgs[m_y:, :m_x, 1] + imgs[:m_y, m_x:, 3]
        final_img[:m_y, m_x:] += imgs[m_y:, m_x:, 1] + imgs[m_y:, :m_x, 2] + imgs[:m_y, :m_x, 5]
        final_img[m_y:, :m_x] += imgs[m_y:, m_x:, 3] + imgs[:m_y, m_x:, 6] + imgs[:m_y, :m_x, 7]
        final_img[m_y:, m_x:] += imgs[m_y:, :m_x, 5] + imgs[:m_y, m_x:, 7] + imgs[:m_y, :m_x, 8]

        final_img /= final_mask

        cv2.imwrite(os.path.join(out_folder, f'{(row * sr_grid_size + col)}.png'), final_img.astype(np.uint16))


def make_collage(in_dir: str, out_img: str, size: int):
    files = natsorted([os.path.join(in_dir, f) for f in os.listdir(in_dir)
                       if os.path.isfile(os.path.join(in_dir, f)) and f.endswith('.png')])

    final_img = None
    for i in range(size):
        line_img = None
        for j in range(size):
            img = cv2.imread(files[i * size + j], cv2.IMREAD_UNCHANGED)
            if line_img is None:
                line_img = np.asarray(img, dtype=np.float64)
            else:
                line_img = np.concatenate((line_img, img), axis=1)
        if final_img is None:
            final_img = line_img
        else:
            final_img = np.concatenate((final_img, line_img), axis=0)

    cv2.imwrite(out_img, final_img.astype(np.uint16))


if __name__ == '__main__':
    run()
