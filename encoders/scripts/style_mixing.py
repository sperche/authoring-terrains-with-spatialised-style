import os
from argparse import Namespace
import re
from typing import List

from tqdm import tqdm
import numpy as np
import torch
import sys
from natsort import natsorted

sys.path.append(".")
sys.path.append("..")

from options.test_options import TestOptions
from models.psp import pSp
import cv2

device = 'cuda' if torch.cuda.is_available() else 'cpu'


# Use --latent_mask to set the style mixing using the notation 1,2,4 for layer 1, 2, 4 or 1-6 for layer 1 to 6. Lower layers represent small resolution.
def run():
    test_opts = TestOptions()
    test_opts.parser.add_argument('--style_image', type=str, help='Path to style image')
    test_opts.parser.add_argument('--apply_style_to_style_image', action='store_true',
                                  help='Apply the style of each image in data_path to the style image (reverse data)')
    test_opts = test_opts.parse()

    if test_opts.resize_factors is not None:
        assert len(
            test_opts.resize_factors.split(',')) == 1, "When running inference, provide a single downsampling factor!"
        out_path_results = os.path.join(test_opts.exp_dir, 'inference_results',
                                        'downsampling_{}'.format(test_opts.resize_factors))
        out_path_coupled = os.path.join(test_opts.exp_dir, 'inference_coupled',
                                        'downsampling_{}'.format(test_opts.resize_factors))
    else:
        out_path_results = os.path.join(test_opts.exp_dir, 'inference_results')
        out_path_coupled = os.path.join(test_opts.exp_dir, 'inference_coupled')

    os.makedirs(out_path_results, exist_ok=True)
    os.makedirs(out_path_coupled, exist_ok=True)

    # update test options with options used during training
    ckpt = torch.load(test_opts.checkpoint_path, map_location='cpu')
    opts = ckpt['opts']
    opts.update(vars(test_opts))
    if 'learn_in_w' not in opts:
        opts['learn_in_w'] = False
    if 'output_size' not in opts:
        opts['output_size'] = 1024
    opts = Namespace(**opts)

    latent_mask = num_range(opts.latent_mask)

    net = pSp(opts).to(device)
    net.eval()

    files = natsorted([os.path.join(opts.data_path, f) for f in os.listdir(opts.data_path)
                       if os.path.isfile(os.path.join(opts.data_path, f)) and f.endswith('.png')])

    style_img = cv2.imread(test_opts.style_image, cv2.IMREAD_UNCHANGED)
    style_latent = get_latent(style_img, net)

    for i, file in enumerate(tqdm(files)):
        img = cv2.imread(file, cv2.IMREAD_UNCHANGED)

        if not opts.apply_style_to_style_image:
            input_img = prepare_img(img)
        else:
            style_latent = get_latent(img, net)
            input_img = prepare_img(style_img)

        result = net(input_img, latent_mask=latent_mask, inject_latent=style_latent,
                     alpha=opts.mix_alpha, resize=opts.resize_outputs)

        result = result[0].cpu().detach().transpose(0, 2).transpose(0, 1).numpy()
        result = normalize(result[:, :, 0], high_range=65535).astype(np.uint16)

        if opts.couple_outputs:
            if not opts.apply_style_to_style_image:
                concatenation = np.concatenate([img, style_img, result], axis=1)
            else:
                concatenation = np.concatenate([style_img, img, result], axis=1)
            cv2.imwrite(os.path.join(out_path_coupled, os.path.basename(file)), concatenation)
        cv2.imwrite(os.path.join(out_path_results, os.path.basename(file)), result)


def get_latent(img: np.ndarray, net: pSp):
    img = prepare_img(img)
    _, latent = net(img, return_latents=True)
    return latent


def prepare_img(img: np.ndarray):
    img = normalize(img.astype(np.float64))
    if img.shape[0] != 256:
        img = cv2.resize(img, (256, 256))
    img = torch.from_numpy(img)

    if len(img.shape) == 2:
        img = torch.unsqueeze(img, 0)

    return torch.unsqueeze(img, 0).float().to(device)


# https://github.com/NVlabs/stylegan2-ada-pytorch/blob/main/generate.py
def num_range(s: str) -> List[int]:
    '''Accept either a comma separated list of numbers 'a,b,c' or a range 'a-c' and return as a list of ints.'''

    range_re = re.compile(r'^(\d+)-(\d+)$')
    m = range_re.match(s)
    if m:
        return list(range(int(m.group(1)), int(m.group(2)) + 1))
    vals = s.split(',')
    return [int(x) for x in vals]


def normalize(array: np.ndarray, high_range: float = 1):
    return ((array - np.min(array)) / (np.max(array) - np.min(array))) * high_range


if __name__ == '__main__':
    run()
