import os
from argparse import Namespace

from tqdm import tqdm
import numpy as np
from PIL import Image
import torch
import sys
import cv2

sys.path.append(".")
sys.path.append("..")

from configs import data_configs
from options.test_options import TestOptions
from models.psp import pSp


# @click.command()
# @click.option('--left_dem', 'first_dem_path', required=True, help='Path of the first DEM.')
# @click.option('--right_dem', 'second_dem_path', required=True, help='Path of the second DEM.')
# @click.option('--out_dir', required=True)
# @click.option('--nb_inference', type=int, default=10, help='Number of images generated in total.')
# @click.option('--make_collage', is_flag=True, help='Make a collage of all inference into one image (default: False)')
# @click.option('--device', type=str, default='cuda')
def run(first_dem_path: str, second_dem_path: str, out_dir: str, nb_inference: int, make_collage: bool, device: str):
    test_opts = TestOptions().parse()

    os.makedirs(out_dir, exist_ok=True)

    # update test options with options used during training
    ckpt = torch.load(test_opts.checkpoint_path, map_location=device)
    opts = ckpt['opts']
    opts.update(vars(test_opts))
    opts = Namespace(**opts)

    net = pSp(opts).to(device)
    net.eval()

    dataset_args = data_configs.DATASETS['dem_encode']
    transforms_dict = dataset_args['transforms'](opts).get_transforms()
    first_dem = load_image(first_dem_path, transforms_dict['transform_inference']).to(device)
    second_dem = load_image(second_dem_path, transforms_dict['transform_inference']).to(device)

    collage = None
    with torch.no_grad():
        _, first_latent = net(first_dem.unsqueeze(0), return_latents=True, resize=opts.resize_outputs, randomize_noise=opts.randomize_noise)
        _, second_latent = net(second_dem.unsqueeze(0), return_latents=True, resize=opts.resize_outputs, randomize_noise=opts.randomize_noise)

        alphas = np.linspace(0, 1, num=nb_inference)
        for i, alpha in tqdm(enumerate(alphas)):
            latent = alpha * second_latent + (1 - alpha) * first_latent
            res = net(latent, input_code=True, resize=opts.resize_outputs, randomize_noise=opts.randomize_noise)

            res = res[0, 0, :, :].detach().cpu().numpy()
            res = (res - np.min(res)) / (np.max(res) - np.min(res))
            res = (res * 65535).astype(np.uint16)

            cv2.imwrite(os.path.join(out_dir, f'{i}.png'), res)

            if make_collage:
                if collage is None:
                    collage = res
                else:
                    collage = np.concatenate((collage, res), axis=1)

        if make_collage:
            cv2.imwrite(os.path.join(out_dir, f'collage.png'), collage)


def load_image(path: str, transform):
    im = Image.open(path)
    im = transform(im).float()
    im = (im - torch.min(im)) / (torch.max(im) - torch.min(im))
    return im


if __name__ == '__main__':
    run(first_dem_path='/your/path/image1.png',
        second_dem_path='/your/path/image2.png',
        out_dir='/your/path',
        make_collage=True,
        nb_inference=100,
        device='cuda')
    # run()
