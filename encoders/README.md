# Authoring Terrains with Spatialised Style

This is the official repository for the *Authoring Terrains with Spatialised Style* paper.
This code is based on *[Encoding in Style: a StyleGAN Encoder for Image-to-Image Translation](https://github.com/eladrich/pixel2style2pixel)* adapted to the official StyleGAN2 implementation, and with our contributions.

# Installation

Set up a new environment with the `environment/terrains_style.yaml` requirements.

# Usage

## Train

The training must be done in two phases. First, train the GradualStyleEncoder:
```
python -m scripts.train.py --dataset_type=dataset_type_defined_in_configs_folder --exp_dir=experiment/your_folder --workers=1 --batch_size=4 --test_batch_size=4 --val_interval=1000 --save_interval=1000 --encoder_type=GradualStyleEncoder --start_from_latent_avg --lpips_lambda=0.8 --l2_lambda=1 --id_lambda=0 --stylegan_weights=pretrained_models/stylegan2_network.pkl --output_size=1024 --label_nc=1 --input_nc=1 --overwrite_exp
```

Then, train our encoder:
```
python -m scripts.train --dataset_type=dataset_type_defined_in_configs_folder --exp_dir=experiment/your_folder --workers=1 --batch_size=4 --test_batch_size=4 --val_interval=1000 --image_interval=100 --save_interval=1000 --encoder_type=CNNSkipInAllF --start_from_latent_avg --lpips_lambda=0.8 --l2_lambda=1 --id_lambda=0 --output_size=1024 --resize_outputs --label_nc=1 --input_nc=1 --checkpoint_path=previously_trained_psp_model.pt
```

## Inference

```
python -m scripts.inference --dataset_type=dataset_type_defined_in_configs_folder --checkpoint_path=your_model/model.pt --test_batch_size=1 --exp_dir=out_folder/ --data_path=your_data_folder/ --couple_outputs

```

## Super-resolution

```
python -m scripts.super_resolution_blend.py --exp_dir=/output/path --checkpoint_path=/path/checkpoint --low_res_image=/low/res/image/path --sr_grid=2 --make_collage [--keep_tmp_folders]
```

## Blender addon

A Blender addon is available [here](https://gitlab.liris.cnrs.fr/sperche/styledem-blender-addon) for easy use.


# Citation
If you find our work useful, please consider citing:

```
@article{perche2023spatialisedstyle,
  author    = {Perche, Simon and Peytavie, Adrien and Benes, Bedrich and Galin, Eric and Guérin, Eric},
  title     = {Authoring Terrains with Spatialised Style},
  journal   = {Pacific Graphics},
  year      = {2023},
}
```

# License

This project is licensed under the MIT License. See the [LICENSE](../LICENSE) file for details.
