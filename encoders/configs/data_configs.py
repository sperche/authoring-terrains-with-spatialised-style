from configs import transforms_config
from configs.paths_config import dataset_paths

DATASETS = {
    'srtm': {
        'transforms': transforms_config.DEMTransforms,
        'train_source_root': dataset_paths['srtm_train'],
        'train_target_root': dataset_paths['srtm_train'],
        'test_source_root': dataset_paths['srtm_test'],
        'test_target_root': dataset_paths['srtm_test'],
    },
}
