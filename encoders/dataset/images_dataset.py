from torch.utils.data import Dataset
from PIL import Image
from utils import data_utils
import torch
from utils.common import normalize_minus_1_1


class ImagesDataset(Dataset):

    def __init__(self, source_root, target_root, opts, target_transform=None, source_transform=None, w_root=None):
        self.source_paths = sorted(data_utils.make_dataset(source_root))
        self.target_paths = sorted(data_utils.make_dataset(target_root))

        self.w_paths = None
        if w_root is not None:
            self.w_paths = sorted(data_utils.make_dataset(w_root))

        self.source_transform = source_transform
        self.target_transform = target_transform
        self.opts = opts

    def __len__(self):
        return len(self.source_paths)

    def __getitem__(self, index):
        from_path = self.source_paths[index]
        from_im = Image.open(from_path)

        to_path = self.target_paths[index]
        to_im = Image.open(to_path)

        if self.target_transform:
            to_im = self.target_transform(to_im)

        if to_im.dtype != torch.float64:
            to_im = to_im.float()

        to_im = normalize_minus_1_1(to_im)

        if self.source_transform:
            from_im = self.source_transform(from_im)
            if from_im.dtype != torch.float64:
                from_im = from_im.float()
            from_im = normalize_minus_1_1(from_im)
        else:
            from_im = to_im

        if self.opts.label_nc == 0:
            from_im = normalize_minus_1_1(from_im)
            if from_im.shape[0] != 3 and len(from_im.shape) == 3:
                from_im = from_im.repeat(3, 1, 1)

        return from_im, to_im
