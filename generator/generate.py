# Copyright (c) 2021, NVIDIA CORPORATION.  All rights reserved.
#
# NVIDIA CORPORATION and its licensors retain all intellectual property
# and proprietary rights in and to this software, related documentation
# and any modifications thereto.  Any use, reproduction, disclosure or
# distribution of this software and related documentation without an express
# license agreement from NVIDIA CORPORATION is strictly prohibited.

"""Generate images using pretrained network pickle."""

import os
import re
from typing import List, Optional

import click
from tqdm import tqdm
import dnnlib
import numpy as np
import imageio
import torch
import time

import legacy

#----------------------------------------------------------------------------

def num_range(s: str) -> List[int]:
    '''Accept either a comma separated list of numbers 'a,b,c' or a range 'a-c' and return as a list of ints.'''

    range_re = re.compile(r'^(\d+)-(\d+)$')
    m = range_re.match(s)
    if m:
        return list(range(int(m.group(1)), int(m.group(2))+1))
    vals = s.split(',')
    return [int(x) for x in vals]

#----------------------------------------------------------------------------

@click.command()
@click.pass_context
@click.option('--network', 'network_pkl', help='Network pickle filename', required=True)
@click.option('--seeds', type=num_range, help='List of random seeds')
@click.option('--trunc', 'truncation_psi', type=float, help='Truncation psi', default=1, show_default=True)
@click.option('--class', 'class_idx', type=int, help='Class label (unconditional if not specified)')
@click.option('--noise-mode', help='Noise mode', type=click.Choice(['const', 'random', 'none']), default='const', show_default=True)
@click.option('--projected-w', help='Projection result file', type=str, metavar='FILE')
@click.option('--projected-w-folder', help='Projection result folder with npz files', type=str)
@click.option('--save-latent-w', help='Save latent as npy file with images', is_flag=True)
@click.option('--outdir', help='Where to save the output images', type=str, required=True, metavar='DIR')
@click.option('--is-grayscale', is_flag=True)
@click.option('--generate-w-plus', help='Generate latent in W+ instead of W', is_flag=True)
def generate_images(
    ctx: click.Context,
    network_pkl: str,
    seeds: Optional[List[int]],
    truncation_psi: float,
    noise_mode: str,
    outdir: str,
    class_idx: Optional[int],
    projected_w: Optional[str],
    projected_w_folder: Optional[str],
    save_latent_w: bool,
    is_grayscale: bool,
    generate_w_plus: bool,
):
    """Generate images using pretrained network pickle.

    Examples:

    \b
    # Generate curated MetFaces images without truncation (Fig.10 left)
    python generate.py --outdir=out --trunc=1 --seeds=85,265,297,849 \\
        --network=https://nvlabs-fi-cdn.nvidia.com/stylegan2-ada-pytorch/pretrained/metfaces.pkl

    \b
    # Generate uncurated MetFaces images with truncation (Fig.12 upper left)
    python generate.py --outdir=out --trunc=0.7 --seeds=600-605 \\
        --network=https://nvlabs-fi-cdn.nvidia.com/stylegan2-ada-pytorch/pretrained/metfaces.pkl

    \b
    # Generate class conditional CIFAR-10 images (Fig.17 left, Car)
    python generate.py --outdir=out --seeds=0-35 --class=1 \\
        --network=https://nvlabs-fi-cdn.nvidia.com/stylegan2-ada-pytorch/pretrained/cifar10.pkl

    \b
    # Render an image from projected W
    python generate.py --outdir=out --projected_w=projected_w.npz \\
        --network=https://nvlabs-fi-cdn.nvidia.com/stylegan2-ada-pytorch/pretrained/metfaces.pkl
    """
    imageio.plugins.freeimage.download()

    print('Loading networks from "%s"...' % network_pkl)
    device = torch.device('cuda')
    with dnnlib.util.open_url(network_pkl) as f:
        G = legacy.load_network_pkl(f)['G_ema'].to(device) # type: ignore

    os.makedirs(outdir, exist_ok=True)

    # Synthesize the result of a W projection.
    if projected_w is not None:
        if seeds is not None:
            print ('warn: --seeds is ignored when using --projected-w')
        print(f'Generating images from projected W "{projected_w}"')
        ws = np.load(projected_w)['w']
        ws = torch.tensor(ws, device=device) # pylint: disable=not-callable
        assert ws.shape[1:] == (G.num_ws, G.w_dim)
        for idx, w in enumerate(ws):
            img = G.synthesis(w.unsqueeze(0), noise_mode=noise_mode)
            img = img.permute(0, 2, 3, 1)[0, :, :, 0].cpu().numpy()
            img = (65535 * (img - np.min(img)) / (np.max(img) - np.min(img))).astype(np.uint16)
            imageio.imwrite(f'{outdir}/proj{idx:02d}.png', img)
            # img = PIL.Image.fromarray(img[0].cpu().numpy(), 'RGB').save(f'{outdir}/proj{idx:02d}.png')
        return

    if projected_w_folder is not None:
        if seeds is not None:
            print ('warn: --seeds is ignored when using --projected-w')
        for filename in tqdm(os.listdir(projected_w_folder)):
            f = os.path.join(projected_w_folder, filename)
            if os.path.isfile(f) and f.endswith('.npz'):
                ws = np.load(f)['w']
                ws = torch.tensor(ws, device=device)
                assert ws.shape[1:] == (G.num_ws, G.w_dim)
                img = G.synthesis(ws, noise_mode=noise_mode)
                img = img.permute(0, 2, 3, 1)[0, :, :, 0].cpu().numpy()
                img = (65535 * (img - np.min(img)) / (np.max(img) - np.min(img))).astype(np.uint16)
                imageio.imwrite(f'{outdir}/{filename.split(".")[0]}_proj.png', img)
        return

    if seeds is None:
        ctx.fail('--seeds option is required when not using --projected-w')

    # Labels.
    label = torch.zeros([1, G.c_dim], device=device)
    if G.c_dim != 0:
        if class_idx is None:
            ctx.fail('Must specify class label with --class when using a conditional network')
        label[:, class_idx] = 1
    else:
        if class_idx is not None:
            print ('warn: --class=lbl ignored when running on an unconditional network')

    # Generate images.
    #all_time=[]
    for seed_idx, seed in enumerate(seeds):
        #start = time.process_time()
        print('Generating image for seed %d (%d/%d) ...' % (seed, seed_idx, len(seeds)))
        z = torch.from_numpy(np.random.RandomState(seed).randn(1, G.z_dim)).to(device)
        if save_latent_w:
            if generate_w_plus:
                z = torch.from_numpy(np.random.RandomState(seed).randn(18, G.z_dim)).to(device)
            w = G.mapping(z, label)

            if generate_w_plus:
                tmp = torch.empty((1,18, G.z_dim)).to(device)
                for i in range(18):
                    tmp[0, i, :] = w[i, 0, :]
                w = tmp
            img = G.synthesis(w, noise_mode=noise_mode)
            np.savez(f'{outdir}/seed{seed:04d}.npz', w=w.cpu().numpy())
        else:
            img = G(z, label, truncation_psi=truncation_psi, noise_mode=noise_mode)
        #stop = time.process_time() - start
        #print(f'Time : {stop}s')
        #all_time.append(stop)

        img = img.permute(0, 2, 3, 1).cpu().numpy()
        if is_grayscale:
            img = img[0, :, :, 0]
        else:
            img = img[0, :, :, :]

        img = (65535 * (img - np.min(img)) / (np.max(img) - np.min(img))).astype(np.uint16)
        # img = (img.permute(0, 2, 3, 1) * 32767.5 + 32768).clamp(0, 65535).cpu().numpy().astype(np.uint16)[0, :, :, 0]
        imageio.imwrite(f'{outdir}/seed{seed:04d}.png', img, format='PNG-FI')
        # PIL.Image.fromarray(img[0, :, :, 0].cpu().numpy(), 'L').save(f'{outdir}/seed{seed:04d}.png')
    #print(all_time)
    #print(np.mean(np.array(all_time)))


#----------------------------------------------------------------------------

if __name__ == "__main__":
    generate_images() # pylint: disable=no-value-for-parameter

#----------------------------------------------------------------------------
