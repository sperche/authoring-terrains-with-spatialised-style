# Authoring Terrains with Spatialised Style

This is the official repository for the *Authoring Terrains with Spatialised Style* paper.
This code is based on *[StyleGAN2-ADA](https://github.com/NVlabs/stylegan2-ada-pytorch)* official implementation

# Usage

## Dataset preparation

To download our data, use the `/data` folder

```
python dataset_tool.py --source folder_containing_img --dest folder/name_of_archive.zip --width 1024 --height 1024
```

## Train
```
python train.py --gpus 1 --outdir results_folder --data ./dataset.zip --resume pkl_file --mirror true --batch 8 --force-rgb false --snap 1 --metrics none
```

## Inference
```
python generate.py --network=path/network.pkl --outdir=out/dir --seeds=0-99
```

# Citation
If you find our work useful, please consider citing:

```
@article{perche2023spatialisedstyle,
  author    = {Perche, Simon and Peytavie, Adrien and Benes, Bedrich and Galin, Eric and Guérin, Eric},
  title     = {Authoring Terrains with Spatialised Style},
  journal   = {Pacific Graphics},
  year      = {2023},
}
```

# License

This project is licensed under the MIT License. See the [LICENSE](../LICENSE) file for details.

