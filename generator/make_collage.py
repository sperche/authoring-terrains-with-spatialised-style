import os
from typing import Optional

from natsort import natsorted
import imageio
import numpy as np
import click


@click.command()
@click.pass_context
@click.option('--in_dir', help='Where to load images', required=True)
@click.option('--out_img', help='Path and name of final image', required=True)
@click.option('--size', help='Number of images in the collage (on one side)', type=int, default=6)
def main(ctx: click.Context, in_dir: str, out_img: str, size: Optional[int]):
    nb_images = size**2
    files = natsorted([os.path.join(in_dir, f) for f in os.listdir(in_dir)
                       if os.path.isfile(os.path.join(in_dir, f))])
    files = files[:nb_images]

    final_img = None
    for i in range(size):
        print(f'Line {i}')
        line_img = None
        for j in range(size):
            img = imageio.imread(files[i*size+j])
            if line_img is None:
                line_img = np.asarray(img)
            else:
                line_img = np.concatenate((line_img, img), axis=0)
        if final_img is None:
            final_img = line_img
        else:
            final_img = np.concatenate((final_img, line_img), axis=1)

    imageio.imwrite(out_img, final_img)


if __name__ == "__main__":
    main()
